
Refer to 
# asd


- [x] **owner:** *requestor by default*
- [x] **environment:** *prod / dev*
- [x] **stack size:** *medium / metal / same as stack xyz*
- [x] **region:** *JP / IE / UK / HK / SG*
- [x] **deploy-kit provisioned:** *yes (default) / no*
- [x] **application permissions and users:** *e.g. manual trader and MJ GUI for quant1 & quant2*
- [x] **mode:** *trade / listen_all / listen_public*
- [x] **stack purpose:** *production / manual / experimental / OTC / other*
- [x] **MJ config:** *default / custom: abc*
- [x] **exchanges:** *Binance / Okx / Huobi / Bitmex / Bitfinex / Coinflex / Kraken / KuCoin / Bybit / Deribit / FTX*
- [x] **traded symbols and communicators:** *e.g. huobi_perpetuals_linear: ETH_USDT_Perpetual_USDT*
- [x] **communicators for listening (for FP calc):** *e.g. huobi_perpetuals_linear*
- [x] **rate limit requirements (Binance):** *low (e.g. dev stacks) / medium (e.g. testing strategy with volatile FP) /
  high (prod)*
- [x] **funds:** *e.g. Huobi: 500 USDT, 0.5 BTC, Okx: 1500 USDT, 0.2 BTC*
